use asterix

-- liste des potions

SELECT * FROM potion;

-- Liste des noms des trophées rapportant 3 points

SELECT * FROM categorie
WHERE NbPoints = 3;

-- 

SELECT(categorie.NomCateg) FROM categorie
WHERE NbPoints = 3;

-- Liste des villages (noms) contenant plus de 35 huttes

SELECT NomVillage, NbHuttes FROM village
WHERE NbHuttes > 35;

-- 

SELECT NomVillage FROM village
WHERE NbHuttes > 35;

-- Liste des trophées (numéros) pris en mai / juin 52.

SELECT * FROM trophee
WHERE YEAR(DatePrise) = 2052 AND (MONTH(DatePrise) = 5 OR MONTH(DatePrise) = 6);

-- 

SELECT(NumTrophee) FROM trophee
WHERE YEAR(DatePrise) = 2052 AND (MONTH(DatePrise) = 5 OR MONTH(DatePrise) = 6);

   
-- Noms des habitants commençant par 'a' et contenant la lettre 'r'.

SELECT * FROM habitant
WHERE Nom LIKE 'a%r%';

-- Numéros des habitants ayant bu les potions numéros 1, 3 ou 4

SELECT habitant.NumHab 
FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
JOIN potion ON absorber.NumPotion = potion.NumPotion
WHERE potion.NumPotion IN ('1', '3', '4')
GROUP BY habitant.NumHab;

-- 
SELECT habitant.NumHab 
FROM habitant, absorber, potion
WHERE habitant.NumHab = absorber.NumHab AND absorber.NumPotion = potion.NumPotion AND potion.NumPotion IN ('1', '3', '4')
GROUP BY habitant.NumHab;


-- Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur

SELECT trophee.NumTrophee, trophee.DatePrise, categorie.NomCateg, habitant.Nom
FROM trophee
JOIN categorie ON categorie.CodeCat = trophee.CodeCat
JOIN habitant ON habitant.NumHab = trophee.NumPreneur;

-- 
SELECT trophee.NumTrophee, trophee.DatePrise, categorie.NomCateg, habitant.Nom
FROM trophee, categorie, habitant
WHERE categorie.CodeCat = trophee.CodeCat AND habitant.NumHab = trophee.NumPreneur;

-- Nom des habitants qui habitent à Aquilona

SELECT habitant.Nom
FROM habitant
JOIN village ON village.NumVillage = habitant.NumVillage
WHERE village.NomVillage = "Aquilona";

-- 
SELECT habitant.Nom
FROM habitant, village
WHERE village.NumVillage = habitant.NumVillage AND village.NomVillage = "Aquilona";

-- Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat

SELECT habitant.Nom
FROM habitant
JOIN trophee ON habitant.NumHab = trophee.NumPreneur
JOIN categorie ON categorie.CodeCat = trophee.CodeCat
WHERE categorie.NomCateg = "Bouclier de Légat";

-- 
SELECT habitant.Nom
FROM habitant, categorie, trophee
WHERE habitant.NumHab = trophee.NumPreneur AND categorie.CodeCat = trophee.CodeCat AND categorie.NomCateg = "Bouclier de Légat";

-- Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant principal

SELECT potion.LibPotion, potion.Formule, potion.ConstituantPrincipal
FROM potion
JOIN fabriquer ON fabriquer.NumPotion = potion.NumPotion
JOIN habitant ON habitant.NumHab = fabriquer.NumHab
WHERE habitant.Nom = "Panoramix";

-- 
SELECT potion.LibPotion, potion.Formule, potion.ConstituantPrincipal
FROM potion, fabriquer, habitant
WHERE fabriquer.NumPotion = potion.NumPotion AND habitant.NumHab = fabriquer.NumHab AND habitant.Nom = "Panoramix";

-- Liste des potions (libellés) absorbées par Homéopatix.

SELECT DISTINCT potion.LibPotion
FROM potion
JOIN absorber ON absorber.NumPotion = potion.NumPotion
JOIN habitant ON habitant.NumHab = absorber.NumHab
WHERE habitant.Nom = "Homéopatix";

-- 
SELECT DISTINCT potion.LibPotion
FROM potion, absorber, habitant
WHERE absorber.NumPotion = potion.NumPotion AND habitant.NumHab = absorber.NumHab AND habitant.Nom = "Homéopatix";

-- Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3

SELECT DISTINCT habitant.Nom
FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion
WHERE fabriquer.NumHab = 3;

-- 

SELECT DISTINCT habitant.Nom
FROM habitant, absorber, fabriquer, potion
WHERE habitant.NumHab = absorber.NumHab AND absorber.NumPotion = fabriquer.NumPotion AND fabriquer.NumHab = 3;

-- Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix.

SELECT DISTINCT habitant.Nom
FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion
JOIN habitant as hf ON fabriquer.NumHab = hf.NumHab
WHERE hf.Nom = "Amnésix";

-- Nom des habitants dont la qualité n'est pas renseignée

SELECT habitant.Nom, NumQualite
FROM habitant
WHERE habitant.NumQualite IS NULL;

-- Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la potion) en février 52.

SELECT DISTINCT habitant.Nom
FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
JOIN potion ON absorber.NumPotion = potion.NumPotion
WHERE potion.LibPotion = "Potion magique n°1" AND YEAR(absorber.DateA) = 2052 AND MONTH(absorber.DateA) = 2;

-- Nom et âge des habitants par ordre alphabétique

SELECT habitant.Nom, habitant.Age
FROM habitant
ORDER BY habitant.Nom ASC;

-- Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village

SELECT resserre.NomResserre, village.NomVillage
FROM resserre
JOIN village ON resserre.NumVillage = village.NumVillage
ORDER BY Superficie DESC;

-- Nombre d'habitants du village numéro 5

SELECT COUNT(*) AS Hab_Vil FROM habitant
WHERE NumVillage = 5;

-- Nombre de points gagnés par Goudurix

SELECT SUM(categorie.NbPoints) AS total_pt 
FROM habitant
JOIN qualite ON habitant.NumQualite = qualite.NumQualite
JOIN trophee ON habitant.NumHab = trophee.NumPreneur
JOIN categorie ON trophee.CodeCat = categorie.CodeCat
WHERE habitant.Nom = "Goudurix";

-- Date de première prise de trophée.

SELECT trophee.DatePrise
FROM trophee
WHERE NumTrophee = 1;

SELECT DATE_FORMAT(trophee.DatePrise, "%D %b %Y") AS new_date
FROM trophee
WHERE NumTrophee = 1;

-- Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées

SELECT SUM(absorber.Quantite) AS lou_nb
FROM absorber
JOIN potion ON absorber.NumPotion = potion.NumPotion
WHERE potion.LibPotion = "Potion magique n°2";

-- Superficie la plus grande

SELECT MAX(Superficie)
FROM resserre;

-- Nombre d'habitants par village (nom du village, nombre)

SELECT village.NomVillage AS name_village, COUNT(*) AS Nbre 
FROM village 
JOIN habitant ON village.NumVillage = habitant.NumVillage
GROUP BY village.NumVillage;

-- Nombre de trophées par habitant

SELECT COUNT(*) AS trophy_nbre, habitant.Nom AS Name_habit
FROM trophee
JOIN habitant ON trophee.NumPreneur = habitant.NumHab
GROUP BY habitant.NumHab;

-- Moyenne d'âge des habitants par province (nom de province, calcul)

SELECT province.NomProvince AS name_pro, AVG(habitant.Age) AS age_moyen
FROM province
JOIN village ON province.NumProvince = village.NumProvince
JOIN habitant ON village.NumVillage = habitant.NumVillage
GROUP BY NomProvince;

-- Nombre de potions différentes absorbées par chaque habitant (nom et nombre)

SELECT habitant.Nom AS nom_hab, COUNT(*) AS nbre_pot
FROM potion
JOIN absorber ON potion.NumPotion = absorber.NumPotion
JOIN habitant ON absorber.NumHab = habitant.NumHab
GROUP BY habitant.Nom;

-- Nom des habitants ayant bu plus de 2 louches de potion zen

SELECT habitant.Nom
FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
JOIN potion ON absorber.NumPotion = potion.NumPotion
WHERE potion.LibPotion = "Potion Zen" AND absorber.Quantite > 2
GROUP BY habitant.Nom;

-- Noms des villages dans lesquels on trouve une resserre

SELECT village.NomVillage
FROM resserre
JOIN village ON resserre.NumVillage = village.NumVillage
GROUP BY village.NomVillage;

-- Nom du village contenant le plus grand nombre de huttes

SELECT village.NomVillage AS call_village
FROM village
WHERE NbHuttes = (SELECT MAX(NbHuttes)
FROM village);

-- Noms des habitants ayant pris plus de trophées qu'Obélix

SELECT COUNT(trophee.NumTrophee) AS t_trophee, habitant.Nom
FROM trophee
JOIN habitant ON trophee.NumPreneur = habitant.NumHab
GROUP BY habitant.Nom;

-- 

SELECT COUNT(trophee.NumTrophee) AS tt
FROM trophee, habitant
WHERE habitant.NumHab = trophee.NumPreneur AND habitant.Nom = "Obélix"
GROUP BY trophee.NumTrophee;

-- 

SELECT COUNT(trophee.NumTrophee) AS top, habitant.Nom
FROM trophee
JOIN habitant ON trophee.NumPreneur = habitant.NumHab
GROUP BY habitant.Nom
HAVING top > (SELECT COUNT(trophee.NumTrophee) AS tt
FROM trophee, habitant
WHERE habitant.NumHab = trophee.NumPreneur AND habitant.Nom = "Obélix");





